import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class Compactador {
	static final int TAMANHO_BUFFER = 4096;
	
	public static void compactar(String arqEntrada, String arqSaida){
		byte[] dados = new byte[TAMANHO_BUFFER];
		int cont;
		BufferedInputStream origem = null;
		FileInputStream streamDeEntrada = null;
		FileOutputStream destino = null;
		ZipOutputStream saida = null;
		ZipEntry entry = null;
		
		try{
			destino = new FileOutputStream(new File(arqSaida));
			saida = new ZipOutputStream(new BufferedOutputStream(destino));
			File file = new File(arqEntrada);
			streamDeEntrada = new FileInputStream(file);
			origem = new BufferedInputStream(streamDeEntrada, TAMANHO_BUFFER);
			entry = new ZipEntry(file.getName());
			saida.putNextEntry(entry);
			while((cont = origem.read(dados,0,TAMANHO_BUFFER)) != -1){
				saida.write(dados,0,cont);
			}
			origem.close();
			saida.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}
