import java.io.File;

public class CompactadorThread {

	static final String pathArquivo = "C:" + File.separator + "compactar" + File.separator;

	public static void main(String[] args) {
		
		File file = new File(pathArquivo);
		GerenciadorDeTempo.inicio = System.currentTimeMillis(); 
				
		for (String arquivo : file.list()) {
			ThreadCompacta compacta = new ThreadCompacta(arquivo, pathArquivo + arquivo, pathArquivo.concat(arquivo).concat(".zip"));
			compacta.start();
		}
	}
}
