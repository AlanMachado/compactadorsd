
public class GerenciadorDeTempo {

	public static long inicio;
	public static int quantidade = 0;
	
	public synchronized static void inicia(String arquivo) {
		quantidade++;
	}
	
	public synchronized static void finaliza(String arquivo) {
		quantidade--;
		
		if (quantidade == 0) {
			long tempo = System.currentTimeMillis() - inicio;
			System.out.println(tempo + " ms");
		}
	}	
}
