
public class ThreadCompacta extends Thread {

	public String nome;
	public String arqEntrada; 
	public String arqSaida; 
	
	public ThreadCompacta(String nome, String arqEntrada, String arqSaida) {
		this.nome = nome;
		this.arqEntrada = arqEntrada;
		this.arqSaida = arqSaida;
	}

	public void run() {
		
		GerenciadorDeTempo.inicia(nome);
		Compactador.compactar(arqEntrada, arqSaida);
		GerenciadorDeTempo.finaliza(nome);
	}
}
