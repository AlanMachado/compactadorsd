import java.io.File;


public class CompactadorSequencial {
	
	
	static final String pathArquivo = "C:"+File.separator+"compactar"+File.separator;
	
	public static void main(String[] args) {
		long tempoInicio = System.currentTimeMillis(); 
		for (String arquivo : args) {
			Compactador.compactar(pathArquivo.concat(arquivo).concat(".txt"), pathArquivo.concat(arquivo).concat(".zip"));
		}
		long tempo = System.currentTimeMillis() - tempoInicio;
		System.out.println(tempo);
	}
}
